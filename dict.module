<?php

/*
 * Description:
 *    This is a dict client module for Drupal.
 *
 * Version:
 *    0.1 - Jan 2, 2006
 *        Dict module for Drupal created
 *
 * Features:
 *    * Provides a form with options for dictionary searches
 *    * A block for quick dictionary searches
 *    * Configurable URL path
 *
 * License: 
 *    GPL, see http://www.gnu.org/copyleft/gpl.txt
 *
 * Author:
 *    Natarajan Kannan (knutties@gmail.com)
 *
 * Credits:
 *    This module uses and borrows heavily from the dict scripts of the
 *    phpdict-2000-06-07.tar.gz module from www.phpclasses.org 
 *    by Jesus M. Castagnetto <jmcastagnetto@zkey.com> 
 * 
 * Drupal:
 *    This module is developed for 4.7.x version of Drupal
 */

include_once './modules/dict/pkg.dict.php';

/* Constants */
define('CONNECT_ERROR', "<b>Could not connect to the dict server</b>");
define('DEFINE_ERROR', "<b>Could not get definitions from dict server</b>");

/**
 * Display help and module information
 * @param section which section of the site we're displaying help 
 * @return help text for section
 */
function dict_help($section='') {
  $output = '';

  switch ($section) {
    case "admin/modules#description":
      $output = t("This is a dictionary module for Drupal");
      break;
  }

  return $output;
}

/**
 * menu hook
 * @param
 * @return array of menu items
 */
function dict_menu() {
	$items = array();
	
	$items[] = array(
		'path' => variable_get('dict_path', 'dict'),
		'title' => t('Dictionary'),
		'callback' => 'dict_all',
		'access' => user_access('access content'),
		'type' => MENU_CALLBACK);

	return $items;
}

/**
 * Valid permissions for this module
 * @param
 * @return An array of valid permissions for the dict module
 */
function dict_perm() {
	return array('access content');
}

/**
 * settings hook
 * @param
 * @return settings form
 */
function dict_settings() {
	// only administrators can access this function
	if (!user_access('access administration pages')) {
	    return message_access();
	}

	// Settings form for configuring the dict server
	$form['dict_server'] = array(
		'#type' => 'textfield',
		'#title' => t('Dict Server hostname/IP address'),
		'#size' => 20,
		'#maxlength' => 64,
		'#default_value' => variable_get('dict_server', 'dict.org'),
		'#description' => t('Dict server hostname'),
	);

	$form['dict_port'] = array(
		'#type' => 'textfield',
		'#title' => t('Dict server port'),
		'#size' => 10,
		'#maxlength' => 64,
		'#default_value' => variable_get('dict_port', '2628'),
		'#description' => t('Dict server port'),
	);

	$form['dict_path'] = array(
		'#type' => 'textfield',
		'#title' => t('Dict access path'),
		'#size' => 40,
		'#maxlength' => 64,
		'#default_value' => variable_get('dict_path', 'dict'),
		'#description' => t('The path to your dict page, relative to
							your Drupal site. Do not include
							the leading slash'),
	);

	return $form;
}

/**
 * block hook
 * @param - op:block operation
 * @return block content for dict
 */
function dict_block($op='list', $delta = 0) {
	if($op === 'list') {
		$block[0]["info"] = t('Dictionary module');
		return $block;
	} else if($op === "view") {
		// Generate the HTML code for the form that will be the block
		$form['word'] = array(
			'#type' => 'textfield',
			'#title' => t('Word'),
			'#size' => 15,
			'#maxlength' => 64,
		);

		$form['database'] = array(
			'#type' => 'hidden',
			'#value' => "*",
		);

		$form['strategy'] = array(
			'#type' => 'hidden',
			'#value' => "exact",
		);

		$form['submit'] = array('#type' => 'submit', '#value' => t('Define'));

		// Generate links based on clean_url settings
        if (!variable_get('clean_url', 0)) {
			$form['#action'] = "?q=" . variable_get('dict_path', 'dict');
		} else {
			$form['#action'] = variable_get('dict_path', 'dict');
		}
			
		$content = drupal_get_form('dict_form', $form);
	}

	$block['subject'] = "Dictionary";
	$block['content'] = $content;
	return $block;
}
	
/**
 * This function generates content for the dict page
 * @param
 * @return
 */
function dict_all() {
	$word = trim($_POST['edit']['word']);
	$database = trim($_POST['edit']['database']);
	$strategy = trim($_POST['edit']['strategy']);

	if($word === "") {
		$word = trim($_GET['word']);
		$database = trim($_GET['database']);
		$strategy = trim($_GET['strategy']);
	}
	
	$out = _dict_form($word);

	if(isset($word) and $word !== "") {
		$out .= _doQuery($word, $database, $strategy);
	}

	print(theme("page", $out));
	return;
}

/**
 * This function handles the generation the dict form for the query, the
 * form is generated dynamically from the server capabilities
 * @param the word to be searched for
 * @form the dict form for querying
 */
function _dict_form($word = "") {
	$form['word'] = array(
		'#type' => 'textfield',
		'#title' => t('word'),
		'#size' => 30,
		'#default_value' => $word,
		'#maxlength' => 64,
		'#description' => t('Enter the word to be searched'),
	);

	$server = variable_get('dict_server', 'dict.org');
	$port = variable_get('dict_port', '2628');

	$server_info = new DictServerInfo($server, $port, true);
	if(!$server_info) {
		$output = "Could not connect to dict server to get information";
		return $output;
	}

	$db_options = $server_info->get_info("databases");
	if(!$db_options) {
		$output = "Could not get databases information";
		return $output;
	}
	// add the wildcard database "Any" to the database array
	$db_options["*"] = "Any";
	$form['database'] = array(
		'#type' => 'select',
		'#title' => t('Select database to search in'),
		'#default_value' => "*",
		'#options' => $db_options,
		'#description' => t('The databases'),
	);

	$strategy_options = $server_info->get_info("strategies");
	$form['strategy'] = array(
		'#type' => 'select',
		'#title' => t('Select strategy for search'),
		'#default_value' => $strategy_options["exact"],
		'#options' => $strategy_options,
		'#description' => t('The search strategy'),
	);

	$form['submit'] = array('#type' => 'submit', '#value' => t('Define'));

	$output = drupal_get_form('dict_form', $form);
	return $output;
}

// The following functions are adapted from 
// Jesus M. Castagnetto's <jmcastagnetto@zkey.com> simplesearch.php script
// that is part of the php_dict package from www.phpclasses.org 
// They have been modified as needed for usage under Drupal

function _mkLinks($str) {
	// The following lines are from the mkLinks function of the simple
	// search script of Jesus M Castagnetto.
	// observed myself another bug with references to URLs - JMC
	// check if it is a HTTP URL or a crossrefence
	$protocols = "/(http|https|ftp|telnet|gopher):\/\/|(mailto|news):/";
	if (preg_match($protocols, $str)) {
		// parse the link and mark it using <>
		$prot1 = "/^((mailto|news):.+)$/";
		$prot2 = "(http|https|ftp|telnet|gopher):\/\/";
		$prot2 = "/^(.*)[ \t\r\n]*\((".$prot2.".+)\)$/";
		if (preg_match($prot1, $str, $regurl)) {
			list ($tmp, $url) = $regurl;
			$desc = $url;
		} elseif (preg_match($prot2, $str, $regurl)) {
			list ($tmp, $desc, $url) = $regurl;
			if ($desc == "")
				$desc = $url;
		}
		$desc = chop($desc);
		$url = chop($url);
		$out = "&lt;<a href=\"$url\">$desc</a>&gt;";
	} else {
		$out = l(
			t($str),
			variable_get('dict_path', 'dict') .
			"&word=" . 
			urlencode(preg_replace('/\s+/', ' ', $str)) . 
			"&database=" . urlencode('*') .
			"&strategy=exact"
		);
	}

	return $out;
}

// perform a query to the server
function _doQuery($str, $db, $strategy) {
	// get server name and port from settings
	$server = variable_get('dict_server', 'dict.org');
	$port = variable_get('dict_port', '2628');

	$query = new DictQuery($server, $port);
	$server_info = new DictServerInfo($server, $port, true);

	if(!$query or !$server_info) {
		$out = CONNECT_ERROR;
		return $out;
	}

	if($strategy === "exact") {
		// this is a request for an exact match, return the list of
		// definitions we get
		if(!$query->define($str, $db)) {
			$out = DEFINE_ERROR;
		} else {
			$res = $query->get("result");
			$out = "<i>Found ". count($res);
			$out .= (count($res) == 1) ? " hit" : " hits";
			$out .= " for " . "<b>$str</b>" . "<br>Database: " 
					. "<b>$db</b>" .  "<br> Strategy: " . "<b>$strategy</b>";
			$out .= "</i><br>\n<dl>\n";

			for ($i=0; $i<count($res); $i++) {
				$entry = $res[$i];
				$out .= "<dt>[" . ($i + 1) . "] : " . $entry["dbname"] . " : " 
						. $entry["term"] .  "</dt>\n";
				
				// HTMLize the < and > characters
				$entry["definition"] = preg_replace('/</', '&lt;',
										$entry["definition"]);
				$entry["definition"] = preg_replace('/>/', '&gt;',
										$entry["definition"]);

				// Create links to words in description
				$link_pattern = "/\{+([^{}]+)\}+/e";
				$out .= "<dd><pre>" . 
						preg_replace($link_pattern, 
						"_mkLinks('\\1')", $entry["definition"]) . 
						"</pre></dd>\n";
			}
			$out .= "</dl>";
		}

	} else {
		// this is a request for a word-match, return the list of
		// matches
		if(!$query->match($str, $strategy, $db)) {
			$out = DEFINE_ERROR;
		} else {
			$res = $query->get("result");
			$out = "<i>Found ". count($res);
			$out .= (count($res) == 1) ? " hit" : " hits";
			$out .= " for " . "<b>$str</b>" . "<br>Database: " 
					. "<b>$db</b>" .  "<br> Strategy: " . "<b>$strategy</b>";
			$out .= "</i><br>\n<dl><pre>\n";

			// Show up a list of matches that a user can chose from
			for($i = 0; $i < count($res); $i++) {
				$entry = $res[$i];
				$link_text = l(t('[' . $entry["term"] . ']'), 
							variable_get('dict_path', 'dict') .
							"&word=" . urlencode($entry["term"]) . 
							"&database=" . urlencode($entry["dbcode"]) .
							"&strategy=exact");

				$dbs_info = $server_info->get_info("databases");
				$db_info = $dbs_info[$entry["dbcode"]];
				$out .= $link_text . " from [$db_info] <br />";
			}
			$out .= "</pre></dl>";
		}
	}

	return $out;
}

?>
